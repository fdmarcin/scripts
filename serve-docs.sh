#!/usr/bin/env bash

# Script for building and previewing GitLab repositories locally.
# It determines where you are and builds either the website or the docs.
# If you run it from any other directory, it builds the docs by default.

# If you're on Mac, change `xdg-open` to `open`

# Functions
serve_gl_docs () {
	echo "You're in the docs."
	echo "Changing to the root level."
	cd ~/dev/gitlab/docs-gitlab-com || exit

	echo "Running: make view"
	make view # serves the site on http://localhost:1313/
}

serve_gl_website () {
	cd ~dev/gitlab/www-gitlab-com/sites/handbook
	echo "You're in the handbook."
	echo "Running: bundle exec middleman"
	NO_CONTRACTS=true bundle exec middleman & xdg-open http://localhost:4567/
}

# Get current directory
myPwd=$(pwd)

# elif statements

# If you're in the website repo, build website
if [[ $myPwd = *dev/gitlab/www-gitlab-com* ]]
then
	serve_gl_website

# If you're in the docs repo, build docs
elif [[ $myPwd = *dev/gitlab/docs-gitlab-com* ]]
then
	serve_gl_docs

# If in any other repository, change to the docs repo and serve that.
else
	echo "You're not in a supported repo."
	echo "Changing to docs-gitlab-com"
	cd ~/dev/gitlab/docs-gitlab-com || exit
	serve_gl_docs
fi
