#!/usr/bin/env bash
# Unofficial bash mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -eu
IFS=$'\n\t'

# Check if VPN is running with:
# nordvpn status | grep "Connected"

vpn_status=$(nordvpn status | grep "Status")

if  [ $vpn_status == "Status: Connected" ]
then
    nordvpn disconnect
    notify-send "Disconnected VPN" || notify-send "Couldn't disconnect VPN"
else
    nordvpn connect pl
    notify-send "Connected to VPN" || notify-send "Couldn't connect to VPN"
fi