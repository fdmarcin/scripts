# Tested with Python 3.8.6
# If refactored to use f-strings, will require at least Python 3.6

# API docs: https://docs.gitlab.com/ee/api/issues.html#new-issue
# Requests docs: https://requests.readthedocs.io/en/latest/
# Initial feedback at https://gitlab.com/-/snippets/2042829

import json
import os
import requests

# Get the GitLab API token from the environment variable
api_token = os.getenv('GITLAB_API_TOKEN')

# Use the Project ID for gitlab-org/gitlab or your personal test project
# gitlab_project_id = "278964"
msj_test_project_id = "15862011"
# TODO: Consider making these environment variables for portability and security

base_url = "https://gitlab.com/api/v4"
full_url = base_url + "/projects/" + msj_test_project_id + "/issues"

# These are empty for now
issue_name = ""
description = ""

headers = {"PRIVATE-TOKEN": api_token}

# The key:value pairs will become issue titles and descriptions
content = {

    "Title 1": """Line 1

    Line 2""",
    "Title 2": """Line 1

    Line 2""",
    }


def create_issues():
    # Create a counter variable
    issues_done = 0

    # Go through each key-value pair of the content dictionary and for each key...
    for key in content:
        payload = {
            # ...use key as title
            "title": key,
            # ...use value as description
            "description": content[key],

            # Labels need to be in one string, separated by commas
            "labels": [
                "label1, label2"
                # "documentation, \
                # devops::plan, \
                # docs::improvement, \
                # Accepting merge requests, \
                # Technical Writing, \
                # good for new contributors"
                ]
                }

        # TODO: test this
        # Add a quick action
        # payload['description'] = payload['description'] + f"\n/assign {owner_username}"

        # Send the API request
        r = requests.post(
            full_url,
            params=payload,
            headers=headers
            )

        # TODO: Check r.status_code, since the API may not create the issue for various reasons.
        #       Or call r.raise_for_status(), to raise an exception for non-2xx responses.

        # Bump the counter
        issues_done += 1

        print("Created: " + key)
        print("    URL: " + json.loads(r.text)["web_url"])
        print("\n")
        # TODO: requests has a built in json and consider using Python's f-strings.
        #       So something like: print(f"    URL: {r.json()['web_url']}")

    print("✅ Created issues:" + str(issues_done))

create_issues()
