#!/usr/bin/env bash
# Unofficial bash mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -eu
IFS=$'\n\t'

# Print output and save to log file
exec > >(tee -i ~/.pull-all-gitlab-repos.log)

# Only save output to log file
# exec 1> ~/.pull-all-gitlab-repos.log 2>&1

# Echo the date so it's in the log file.
date --rfc-3339 s

# ┌────────────┐
# │ Sync repos │
# └────────────┘

get_default_branch () {
	git remote show origin | grep 'HEAD branch' | cut -d' ' -f5
}


for i in ~/dev/gitlab/*/.git; do
	echo -e "\n\e[33m$(dirname "$i")\e[0m"
	cd "$i"/.. || notify-send "Couldn't open $i" --icon="$HOME/Pictures/icons/thinking.png"

	echo -e "\n\e[36m-- Pulling $(get_default_branch).\e[0m"
	git checkout "$(get_default_branch)"
	git pull --ff-only --prune || notify-send "Couldn't pull $i" --icon="$HOME/Pictures/icons/thinking.png"

	# Run git garbage collection
	echo -e "\n\e[36m-- Collecting garbage.\e[0m"
	# git prune || true
	git gc --auto || true
done

# Send a success notification (Linux only)
# notify-send "Pulled all repos" --icon="gitlab"

# ┌─────────────────────┐
# │ Update dependencies │
# └─────────────────────┘

dirs_to_update_dependencies=(
	"$HOME/dev/gitlab/gitlab-development-kit/gitlab"
	"$HOME/dev/gitlab/gitlab-development-kit"
	)

# Sync dependencies
for i in "${dirs_to_update_dependencies[@]}"; do
	REPONAME=$(echo "$i" | sed 's:.*/::')

	echo -e "\n\e[36m-- [$REPONAME] Syncing dependencies\e[0m"
	cd $i || notify-send "[$REPONAME] Couldn't open" --icon="$HOME/Pictures/icons/thinking.png"

	echo "Running: mise install"
	mise install || notify-send "[$REPONAME] Couldn't update mise" --icon="$HOME/Pictures/icons/thinking.png"

	echo -e "\n\e[33m--- Running: bundle install\e[0m"
	bundle install | grep -v Using || notify-send "[$REPONAME] Couldn't find Bundle" --icon="$HOME/Pictures/icons/thinking.png"


	echo -e "\n\e[33m--- Running: yarn install\e[0m"
	yarn install || true

done

# Update dependencies in the docs project
echo -e "\n\e[36m-- [docs-gitlab-com] Syncing dependencies\e[0m"
cd "$HOME/dev/gitlab/docs-gitlab-com"
yes | make setup || notify-send "[docs-gitlab-com] Couldn't sync dependencies" --icon="$HOME/Pictures/icons/thinking.png"

# ┌────────────┐
# │ Update GDK │
# └────────────┘

cd "$HOME/dev/gitlab/gitlab-development-kit"
# notify-send "Updating GDK" --icon="gitlab" # Linux only
gdk update || notify-send "[GDK] Couldn't update GDK" --icon="$HOME/Pictures/icons/thinking.png"

# Migrate db and drop the diff
# cd "$HOME/dev/gitlab/gitlab-development-kit/gitlab"
# gdk start postgresql
# ../support/bundle-exec rails db:migrate
# git status
# git restore db/*
# gdk stop

# # Send a success notification (Linux only)
# notify-send "GDK is up to date" --icon="gitlab"


# Send a success notification (Linux only)
notify-send "All done" --icon="$HOME/Pictures/icons/all-the-things.jpg"
